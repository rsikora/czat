# -*- coding: utf-8 -*-
import os
from flask import *
import requests
import datetime
import couchdb
import urllib2

app = Flask(__name__)
app.config.from_object(__name__)
app.secret_key = 'sekretnyKlucz123'

dane = {"uzytkownicy":[], "wiadomosci":[]}
serwer = couchdb.Server('http://194.29.175.241:5984/')
bazaserwerow = serwer['chat']


def zapiszBaze():
    baza=open(os.path.join('static', 'bazadanych.json'), "w+")
    baza.write(json.dumps(dane))

def wczytajBaze():
    baza=open(os.path.join('static', 'bazadanych.json'), "r+")

def aktywneSerwery():
    adresy = dict()
    for doc in bazaserwerow:
        id = bazaserwerow[doc]
        ip = ''
        add = False
        delivery = ''
        if id.has_key('host') and id.has_key('active') and id.has_key('delivery'):
            ip = id['host']
            add = id['active']
            delivery = id['delivery']
        if add is True:
            adresy[ip] = delivery
    return adresy


def aktywacja():
    for id in bazaserwerow:
        doc = bazaserwerow.get(id)
        if doc.has_key('host') and doc['host'] == 'http://egzamin.herokuapp.com':
            if doc['active'] is False:
                bazaserwerow.delete(doc)
                bazaserwerow.create({'host': 'http://egzamin.herokuapp.com', 'active': True, 'delivery': '/odbierz/'})
            #else:
            #    bazaserwerow.delete(doc)
            #    bazaserwerow.create({'host': 'egzamin.herokuapp.com', 'active': False, 'delivery': 'odbierz/'})

def czyDodac():
    adresy = aktywneSerwery()
    for adres in adresy:
        if adres == 'http://egzamin.herokuapp.com':
            return False
    return True

def dodajSerwer():
    bazaserwerow.create({'host': 'http://egzamin.herokuapp.com', 'active': True, 'delivery': '/odbierz/'})

def czyDobryURL(url):
    try:
        urllib2.urlopen(urllib2.Request(url))
        return True
    except:
        return False

@app.route('/')
def index():
    zalogowany = False
    if "logged" in session:
        zalogowany = True
    uzytkownicy = dane["uzytkownicy"]
    return render_template('glowna.html', logged=zalogowany, users=uzytkownicy)


@app.route('/zaloguj', methods=['GET', 'POST'])
def zaloguj():
    if request.method == "GET":
        informacja = ""
        if "logged" in session:
            return redirect("")
        return render_template('login.html', informacja=informacja)
    if request.method == "POST":
        login = request.form['loginName']
        if login in dane["uzytkownicy"]:
            informacja = "Nazwa jest juz zajeta!"
            return render_template('login.html', informacja=informacja)
        else:
            dane["uzytkownicy"].append(login)
            session['logged'] = True
            session['loggedUser'] = login
            return redirect("")


@app.route('/wyloguj')
def wyloguj():
    informacja = "Blad"
    if "logged" in session:
        if session["loggedUser"] in dane["uzytkownicy"]:
            dane["uzytkownicy"].remove(session["loggedUser"])
        session.pop('logged', None)
        session.pop('loggedUser', None)
        informacja = "Wylogowano!"
    return render_template('glowna.html', informacja=informacja, logged=False)

@app.route('/czat')
def czat():
    if "logged" in session:
        uzytkownicy = dane["uzytkownicy"]
        wiadomosci = dane["wiadomosci"]
        return render_template('czat.html', uzytkownicy=uzytkownicy, wiadomosci=wiadomosci)
    return render_template('login.html')

@app.route('/wyslij', methods=['POST'])
def wyslij():
    if request.method == "POST":
        if "logged" in session:
            wiadomosc = request.form['wiadomosc']
            czas = datetime.datetime.now().time().replace(microsecond=0)
            dane["wiadomosci"].append(str(czas)+" "+session["loggedUser"]+": "+wiadomosc)
            wpis = {'message': wiadomosc, 'user': session["loggedUser"], 'timestamp': str(czas)}
            adresy = aktywneSerwery()
            headers = {'content-type': 'application/json'}
            for adres in adresy:
                if adres == 'http://egzamin.herokuapp.com':
                    next()
                url = adres+adresy[adres]
            #url = 'http://127.0.0.1:5000/odbierz'
                if czyDobryURL(url):
                    requests.post(url, data=json.dumps(wpis), headers=headers)
            return redirect("")


@app.route('/odbierz', methods=['POST'])
def odbierz():
    adresy = aktywneSerwery()
    for adres in adresy:
        if adres == 'http://egzamin.herokuapp.com':
            next()
        url = adres+adresy[adres]
        if czyDobryURL(url):
            zadanie = requests.get('http://egzamin.herokuapp.com').Request
            if zadanie.method == "POST":
                if zadanie.headers['Content-Type'] == 'application/json':
                    try:
                        data = json.loads(zadanie.data)
                        user = data['user']
                        message = data['message']
                        timestamp = data['timestamp']
                        dane["wiadomosci"].append(timestamp+" "+user+": "+message)
                    except (ValueError, KeyError, TypeError):
                        print 'blad'
                        # Not valid information, bail out and return an error
    return redirect("")


if __name__ == '__main__':
    if czyDodac():
        dodajSerwer()
    else:
        aktywacja()
    app.run(debug=True)

