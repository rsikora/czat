class UserDB:
    def users(self):
        users = [dict(id=id, title=database[id]['user']) for id in database.keys()]
        return users

    def newUser(self, user):
        baza = database
        for id in database.keys():
            if not(user == database[id]['user']):
                id = len(baza.items())+1
                baza.update({id: {'user': user}})
                return True
        return False

database = {}